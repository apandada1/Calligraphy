<div align="center">
  <img alt="An ink bottle in the style of GNOME icons" width="160" src="./data/icons/hicolor/scalable/apps/dev.geopjr.Calligraphy.svg">
  <h1>Calligraphy</h1>
  <h3>Turn text into ASCII banners</h3>
  <img alt="" src="./data/screenshots/entry.png">
  <a href='https://flathub.org/apps/details/dev.geopjr.Calligraphy'>
    <img width='192' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/>
  </a>
</div>

  Also available on the [AUR](https://aur.archlinux.org/packages/calligraphy).

# Building

1. Install and open [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder)
2. Select "Clone Repository…"
3. Clone `https://gitlab.gnome.org/GeopJr/Calligraphy.git`
4. Run the project with the ▶ button at the top, or by pressing <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>Space</kbd>

# Sponsors

<div align="center">

[![GeopJr Sponsors](https://cdn.jsdelivr.net/gh/GeopJr/GeopJr@main/sponsors.svg)](https://github.com/sponsors/GeopJr)

</div>

# Acknowledgements

- This is a fork of [Calligraphy](https://gitlab.gnome.org/World/Calligraphy) by [Gregor "gregorni" Niehl](https://gitlab.gnome.org/gregorni)
- Calligraphy uses [pyfiglet](https://github.com/pwaller/pyfiglet) to turn text into impressive banners made up of ASCII Characters

# Contributing

1. Read the [Code of Conduct](./CODE_OF_CONDUCT.md)
2. Fork it ( https://gitlab.gnome.org/GeopJr/Calligraphy/-/forks/new )
3. Create your feature branch (git checkout -b my-new-feature)
4. Commit your changes (git commit -am 'Add some feature')
5. Push to the branch (git push origin my-new-feature)
6. Create a new Pull Request
